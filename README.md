# Red Devil I2C

Small sample program that controls the LEDs on my GPU (PowerColor RedDevil 5700XT).

**This program is untested on your hardware. I2C can screw things up if you're not careful. This code comes with no warranty, and I take no responsibility for any GPU bricks or other defects.**

### Development Instructions

- Install Visual Studio 2022 stuff for C compiling (can't remember the specifics)
- In Windows Terminal, launch a tab with the "Developer Command Prompt for VS 2022" profile
- Clone this repo and `cd` into it
- `code .`

### See also

https://gpuopen-librariesandsdks.github.io/adl/index.html
https://gpuopen-librariesandsdks.github.io/adl/group__I2CDDCEDIDAPI.html