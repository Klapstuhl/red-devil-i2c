# PowerColor RedDevil 5700XT I2C protocol

### Overview

Tested with atiadlxx.dll SysWOW64 dll with cdecl. Everything is decimal unless denoted by 0x prefix.

DevilZone is the official LED control application.

**Note:** After writing, you have to Sleep for some ms. I use 100ms, DevilZone uses *roughly* 32ms. If you're too fast, you'll break it, and even DevilZone won't have any effect. You must sleep and wake and try again.

### Reading/Writing I2C

Call this function.

```
ADL_Display_WriteAndReadI2C(int iAdapterIndex, ADLI2C *plI2C)
```

`iAdapterIndex` must be 0, and `plI2C` should be populated as below.

### ADLI2C Structure Format

`struct ADLI2C` is this:
- iSize = 32
- iLine = 1
- iAddress = 68 if reading, 69 if writing
- iOffset
  - These offsets can be *written* to:
    - #1 - settings
      - Sets animation mode, brightness, and animation speed.
    - #2 &ndash; #12 - individual color
      - Sets color for an individual LED on the LED line, physically arranged on the card from right to left. In addition, the last 3 offsets (10,11,12) are mirrored to the RedDevil logo on the top of the card, from *left* to *right*.
    - #14 - all color
      - Sets colors for every LED (same as offsets #2 thru #12).
    - #15 - animation color
      - Sets the color used for animation modes 7 (meteor) and 8 (ripple)
  - These offsets can be *read* from:
    - #129 - settings
    - #130 - color of rightmost LED
    - On startup and on every window refocus, DevilZone reads from offset #129 then #130. Probably to set the colored logo in the center of the GUI.
  - AFAICT, in general, add 128 to a writable offset to get an offset that can be *read* from
- iAction = ADL_DL_I2C_ACTIONREAD_REPEATEDSTART (3) or ADL_DL_I2C_ACTIONWRITE (2)
- iSpeed = 150
- iDataSize = 3
- pcData = Always 3 bytes. Value read/written depends on the type of data the offset holds.
  - settings
    - Byte 0 - mode
      - 0 off
      - 1 single color
      - 2 breathing
      - 3 neon (a smooth color shift)
      - 4 blink
      - 5 double blink
      - 6 color shift (a stepped color shift)
      - 7 meteor
      - 8 ripple
      - 9 seven colors (cycles between Blue/Cyan/Pink/Red/Yellow/LtGreen/Green)
      - **Note**: Modes 3, 6, and 9 will ignore values in the individual color offsets. Modes 7 and 8 will also ignore those, but will instead use the special color offsets.
    - Byte 1 - brightness. 100 is full bright, 0 is off.
    - Byte 2 - animation speed. 0 is fastest and 100 is slowest.
  - color
    - Byte 0 - unsigned byte Red
    - Byte 1 - unsigned byte Green
    - Byte 2 - unsigned byte Blue

### Examples

- Setting single color, light blue
  - settings#1 - [1, 100, 5]
  - color#14 - [128, 128, 255]
  - AFAICT, setting color#2 can be omitted
- Setting breathing, blue, at the lowest speed
  - settings#1 - [2, 100, 100]
  - color#14 - [0, 0, 255]
  - color#2 - [0, 0, 255]
- Setting single color, magenta, at full brightness
  - settings#1 - [1, 100, 100]
  - color#14 - [255, 0, 255]
  - color#2 - [255, 0, 255]
  - For off-white, both colors are [255, 254, 253]
- Setting single color, red, at lowest brightness
  - settings#1 - [1, 0, 100]
  - color#14 - [255, 0, 0]
  - color#2 - [255, 0, 0]
  - For half brightness, settings is [1, 47, 100]
- Setting breathing, red, at the highest speed
  - settings#1 - [2, 100, 0]
  - color#14 - [255, 0, 0]
  - color#2 - [255, 0, 0]
- Setting neon, at ~75% brightness, at ~50% speed
  - settings#1 - [3, 80, 49]
  - Color offsets are not written to.
  - For full speed, settings is [3, 80, 0]
  - For slow speed, settings is [3, 80, 100]
  - For full bright, settings is [3, 100, 100]
- Setting blink, at full brightness & speed
  - settings#1 - [4, 100, 0]
- Setting double blink, at full b&s
  - settings#1 - [5, 100, 0]
  - For lowest b&s, settings is [5, 0, 100]
- Setting color shift, at full brightness & speed (really crazy)
  - settings#1 - [6, 100, 0]
  - Color offsets are not written to.
- Setting meteor, orange, at full b&s
  - settings#1 - [7, 100, 1]
  - spcolor#15 - [255, 127, 0]
- Setting ripple, red, at full brightness, at half speed
  - settings#1 - [8, 100, 49]
  - spcolor#15 - [255, 0, 0]
- Setting seven colors, at full b&s
  - settings#1 - [9, 100, 0]
  - Color offsets are not written to.

### Hardware Monitor Mode

Hardware monitor mode is actually implemented with single color. That means it needs an external app to control it.

For hardware monitor, based on the CPU load %, at full brightness, DevilZone will do this:

Once every 2 seconds, offsets #2 through #12 are written red [255, 0, 0] or black [0, 0, 0], depending on the CPU load %. Occasionally, settings#1 is written [1, 100, 1].

The DevilZone app is buggy. After applying hardware monitor mode for the first time in the app's lifetime, refocusing the window will always set the mode back to hardware monitor mode. For example, when having the card's mode set to blink, opening the app, setting hardware monitor, then setting breathing, then alt-tabbing out then in, the app will show hardware monitor as selected. From here, it will begin firing off colors to the individual LED offsets, as if it were still in hardware monitor mode. I'm not sure why this bug happens. I guess since hardware monitor is implemented as single color (1), the app is programmed to stop trusting the mode at settings#129 until app shutdown, because otherwise a window refocus would have the app show single color.